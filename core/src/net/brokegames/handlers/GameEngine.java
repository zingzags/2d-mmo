/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.handlers;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.World;

import net.brokegames.handlers.components.PlayerComponent;
import net.brokegames.handlers.components.PositionComponent;
import net.brokegames.handlers.components.RenderableComponent;
import net.brokegames.handlers.components.SpriteComponent;
import net.brokegames.handlers.systems.RenderSystem;

/**
 * @author Steven
 *
 */

public class GameEngine {

	private Engine engine;
	
	public GameEngine(Engine engine, SpriteBatch batch, World world) {
		this.engine = engine;
		RenderSystem renderSystem = new RenderSystem(batch);
		engine.addSystem(renderSystem);
		
		Entity en = new Entity();
		en.add(new PositionComponent(0, 0))
			.add(new RenderableComponent())
			.add(new SpriteComponent(new Texture("characters/hatboy.png")))
			.add(new PlayerComponent());
		engine.addEntity(en);
	}
	
	
	public void addEntity(Entity entity) {
		engine.addEntity(entity);
	}
	
	public void update() {
		engine.update(Gdx.graphics.getDeltaTime());
	}
	
	
	public void dispose() {
		//Once I change the ways the entities are going to be rendered, this method would dispose of all entities if need be
	}

}

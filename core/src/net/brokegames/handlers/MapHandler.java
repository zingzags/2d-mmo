package net.brokegames.handlers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;

public class MapHandler {
	
    private static int lvlTileWidth;
    private static int lvlTileHeight;
    private static int lvlPixelWidth;
    private static int lvlPixelHeight;
    private static int tilePixelWidth;
    private static int tilePixelHeight;
    private static TiledMap tiledMap;
    private static SoundHandler sound;
    
    public static void loadLevel(String fileName) {
        tiledMap = new TmxMapLoader().load(Gdx.files.internal("maps/" + fileName + ".tmx").toString());
        MapProperties properties = tiledMap.getProperties();
        lvlTileWidth = properties.get("width", Integer.class);
        lvlTileHeight = properties.get("height", Integer.class);
        tilePixelWidth = properties.get("tilewidth", Integer.class);
        tilePixelHeight = properties.get("tileheight", Integer.class);
        lvlPixelWidth = lvlTileWidth * tilePixelWidth;
        lvlPixelHeight = lvlTileHeight * tilePixelHeight;
        if (properties.get("sound") != null) {
        	int volume = 50;
        	int pitch = 50;
        	int pan = 50;
        	if (properties.get("volume") != null) {
        		volume = properties.get("volume", Integer.class);
        	}
        	if (properties.get("pitch") != null) {
        		pitch = properties.get("pitch", Integer.class);
        	}
        	if (properties.get("pan") != null) {
        		pan = properties.get("pan", Integer.class);
        	}        	
        	sound.playSound(properties.get("sound", String.class), volume, pitch, pan);
        }
    }

	/**
	 * @return the lvlTileWidth
	 */
	public static int getLvlTileWidth() {
		return lvlTileWidth;
	}

	/**
	 * @return the lvlTileHeight
	 */
	public static int getLvlTileHeight() {
		return lvlTileHeight;
	}

	/**
	 * @return the lvlPixelWidth
	 */
	public static int getLvlPixelWidth() {
		return lvlPixelWidth;
	}

	/**
	 * @return the lvlPixelHeight
	 */
	public static int getLvlPixelHeight() {
		return lvlPixelHeight;
	}

	/**
	 * @return the tilePixelWidth
	 */
	public static int getTilePixelWidth() {
		return tilePixelWidth;
	}

	/**
	 * @return the tilePixelHeight
	 */
	public static int getTilePixelHeight() {
		return tilePixelHeight;
	}

	/**
	 * @return the tiledMap
	 */
	public static TiledMap getTiledMap() {
		return tiledMap;
	}
	
	/**
	 * @return the audio which is being played
	 */
	public static SoundHandler getSound() {
		return sound;
	}	

}
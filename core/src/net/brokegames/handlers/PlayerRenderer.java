package net.brokegames.handlers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

public class PlayerRenderer {

	private int columns = 5, rows = 1;
	private TextureRegion currentFrame;
	private Texture texture;
	private Animation animation;
	private String player;
	private int moveAnimation = 0;
	private int currentDirection;
	private Vector2 targetPosition;
	private Vector2 position;
	private float speed = 50f;
	public float delta;
	private Body body;
	private Fixture fixture;
	
	public PlayerRenderer(String player) {
		super();
		TextureRegion[] frames = new TextureRegion[columns];
		targetPosition = new Vector2();
		position = new Vector2();
		this.player = player;
		try {
			 texture = new Texture(Gdx.files.internal("characters/" + this.player + ".png").toString());
		} catch (Exception e) {
			// new ExceptionHandler(this.getClass().getName(), e);
		}
		TextureRegion[][] textureRegion = TextureRegion.split(texture, texture.getWidth() / columns, texture.getHeight() / rows);
		int index = 0;
		for (int j = 0; j < columns; j++) {
			frames[index++] = textureRegion[0][j];
		}
		animation = new Animation(1f, frames);
		animation.setPlayMode(PlayMode.NORMAL);
		currentFrame = animation.getKeyFrame(0, false);
		position.set(100, 1000);
		targetPosition.set(0, 0);
	}
	
	public void draw(Batch batch, MapHandler map) {
		if (body == null) {
			BodyDef bodydef = new BodyDef(); //This is where we create the Physics object that will be manipulated
			bodydef.type = BodyDef.BodyType.DynamicBody;
			bodydef.position.set(position);
			PolygonShape shape = new PolygonShape();
			/*
			 * This is where we define the center of our sprite,
			 * the center is where all the physics is going to be calculated, and applied from 
			 * */
			shape.setAsBox((texture.getWidth() / columns) / 2, (texture.getHeight() / rows) / 2);
			/*
			 * FixtureDef is where you define the physics of the object in question
			 * like the density of the object
			 * */
			FixtureDef fixtureDef = new FixtureDef();
	        fixtureDef.shape = shape;
	        fixtureDef.density = 0f;
	        fixtureDef.friction = .5f;
	        //fixtureDef.restitution = .2f;
			// Create a body in the world using our definition
			//body = map.getWorld().createBody(bodydef);
			fixture = body.createFixture(fixtureDef);
			shape.dispose();
		}
		position.set(body.getPosition().x, body.getPosition().y);
	//	map.getCamera().position.set(getX(), getY(), 0);
	//	map.getCamera().update();
	//	batch.setProjectionMatrix(map.getCamera().combined);
		currentFrame = animation.getKeyFrame(moveAnimation, false);
		batch.draw(currentFrame, getX(), getY());
	}
	
	
	//private boolean movement() {
		//return 
	//}
	
	public double movementOverTime() {
		return Math.ceil(speed * Gdx.graphics.getDeltaTime());
	}	
	
	
	public boolean translatingX() {
		return targetPosition.x != getX();
	}

	
	public boolean translatingY() {
		return targetPosition.y != getY();
	}
	
	public boolean isTranslating() {
		return translatingX() || translatingY();
	}

	
	public void setMoveAnimation(int moveAnimation) {
		this.moveAnimation = moveAnimation;
	}

	
	public int getMoveAnimation() {
		return moveAnimation;
	}

	
	public boolean isFacingDown() {
		return currentDirection == MovementHandler.DOWN.getDirection();
	}

	
	public boolean isFacingLeft() {
		return currentDirection == MovementHandler.LEFT.getDirection();
	}

	
	public boolean isFacingRight() {
		return currentDirection == MovementHandler.RIGHT.getDirection();
	}

	
	public boolean isFacingUp() {
		return currentDirection == MovementHandler.UP.getDirection();
	}
	
	public float getX() {
		return position.x;
	}

	
	public float getY() {
		return position.y;
	}

	
	public void setX(float x) {
		position.set(getX() + x, position.y);
	}

	
	public void setY(float y) {
		position.set(position.x, getY() + y);
	}

	
	public Vector2 getPosition() {
		return position;
	}
	
	
	public Vector2 getTargetPosition() {
		return targetPosition;
	}

	public void dispose() {
		texture.dispose();
	}
	
	public enum MovementHandler {

		DOWN(0),
		LEFT(4),
		RIGHT(8),
		UP(12);
		
		private int direction;
		
		private MovementHandler(int direction) {
			this.direction = direction;
		}
		
		public int getDirection() {
			return direction;
		}
	}
	
	/**
	 * @return the fixture
	 */
	public Fixture getFixture() {
		return fixture;
	}

	/**
	 * @param fixture the fixture to set
	 */
	public void setFixture(Fixture fixture) {
		this.fixture = fixture;
	}
	
}

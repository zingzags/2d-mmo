package net.brokegames.handlers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;

/**
* Created by Phil on 1/5/2015.
*/
public class SoundHandler {
	
    private Sound music;
    private Sound sfx;
    private float masterVolume = 1.0f;
    private OrthographicCamera camera;

    public void setCamera(OrthographicCamera camera) {
        this.camera = camera;
    }

    public void playMusic(String fileName, float volume) {
        playMusic(fileName, volume, 1.0f, 0.0f);
    }

    public void playMusic(String fileName, float volume, float pitch, float pan) {
        if (music != null) {
            music.stop();
            music.dispose();
        }
        music = Gdx.audio.newSound(Gdx.files.internal("music/" + fileName));
        if (music != null)
        	music.loop(volume * masterVolume, pitch, pan);
    }

    public void playSound(String fileName, float volume) {
        playSound(fileName, volume, 1.0f, 0.0f);
    }

    public void playSoundAtPosition(String fileName, float baseVolume, Vector3 sourcePos) {
        float boundary = camera.viewportWidth / 2;
        float xDistance = sourcePos.x - camera.position.x;
        float distance = camera.position.dst(sourcePos);
        distance = Math.min(distance, boundary);

        playSound(fileName, baseVolume * masterVolume * (1 - distance / boundary), 1.0f, xDistance / boundary);
    }

    public void playSound(String fileName, float volume, float pitch, float pan) {
        if (sfx != null) {
            sfx.stop();
            sfx.dispose();
        }
        sfx = Gdx.audio.newSound(Gdx.files.internal("music/" + fileName));
        if (sfx != null)
        	sfx.play(volume, pitch, pan);
    }

    public void dispose() {
        if (music != null)
            music.dispose();
        if (sfx != null)
            sfx.dispose();
    }
}

/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.handlers.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.physics.box2d.Fixture;

import net.brokegames.handlers.MapHandler;

/**
 * @author Steven
 *
 */
public class EntityComponent extends Component {

	private Fixture fixture;
	private MapHandler map;
	
	public EntityComponent() {
		
	}
	
	/**
	 * @return the fixture
	 */
	public Fixture getFixture() {
		return fixture;
	}

	/**
	 * @param fixture the fixture to set
	 */
	public void setFixture(Fixture fixture) {
		this.fixture = fixture;
	}

	/**
	 * @return the map
	 */
	public MapHandler getMap() {
		return map;
	}

	/**
	 * @param map the map to set
	 */
	public void setMap(MapHandler map) {
		this.map = map;
	}
	
	
}

/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.handlers.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector3;

/**
 * @author Steven
 *
 */
public class PositionComponent extends Component {

	public Vector3 position;
	
	public PositionComponent() {}
	
	public PositionComponent(Vector3 position) {
		this.position = position;
	}
	
	public PositionComponent(float x, float y) {
		position = new Vector3(x, y, 0);
	}
	
	public PositionComponent(float x, float y, float z) {
		position = new Vector3(x, y, z);
	}
}

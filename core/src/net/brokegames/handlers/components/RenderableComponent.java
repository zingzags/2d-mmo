/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.handlers.components;

import com.badlogic.ashley.core.Component;

/**
 * @author Steven
 *
 */
public class RenderableComponent extends Component {
	//Entities which has this component can be rendered
}

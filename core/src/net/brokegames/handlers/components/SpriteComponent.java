/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.handlers.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.utils.Array;

/**
 * @author Steven
 *
 */
public class SpriteComponent extends Component {
	
    private Array<Sprite> sprites = new Array<Sprite>();

    public SpriteComponent(Texture...textures) {
        for (Texture texture : textures)
            sprites.add(new Sprite(texture));
    }
    
    public Array<Sprite> getSprites() {
    	return sprites;
    }

}

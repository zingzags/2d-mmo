/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.handlers.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.SortedIteratingSystem;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import net.brokegames.handlers.components.PositionComponent;
import net.brokegames.handlers.components.RenderableComponent;
import net.brokegames.handlers.components.SpriteComponent;

/*
 * 
 * @author Steven
 *
 */
public class RenderSystem extends SortedIteratingSystem {
	
	private SpriteBatch batch;
	private ComponentMapper<SpriteComponent> spriteMap = ComponentMapper.getFor(SpriteComponent.class);	
	
	public RenderSystem(SpriteBatch batch) {
		super(Family.all(RenderableComponent.class, SpriteComponent.class, PositionComponent.class).get(), new ZComparator());
		this.batch = batch;
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime) {
    	for (Sprite sprites : spriteMap.get(entity).getSprites()) {
    		PositionComponent pos = entity.getComponent(PositionComponent.class);
    		sprites.setPosition(pos.position.x, pos.position.y);
    		sprites.draw(batch);
    	}
	}	
	
}

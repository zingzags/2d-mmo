package net.brokegames.handlers.systems;

import java.util.Comparator;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;

import net.brokegames.handlers.components.PositionComponent;

/**
 * Created by Phil on 3/3/2015.
 */
public class ZComparator implements Comparator<Entity> {
    private ComponentMapper<PositionComponent> posMap = ComponentMapper.getFor(PositionComponent.class);

    @Override
    public int compare(Entity e1, Entity e2) {
        return (int)Math.signum(posMap.get(e1).position.z - posMap.get(e2).position.z);
    }
}

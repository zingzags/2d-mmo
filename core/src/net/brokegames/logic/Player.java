/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.logic;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;

import net.brokegames.handlers.PlayerRenderer;

/**
 * @author Steven
 *
 */

public class Player extends PlayerRenderer implements InputProcessor {

	/**
	 * @param String player
	 * This is the method constructor for the Player Class.
	 * It requires a String with the name of the asset which you are trying to access
	 * in "assets/characters/".
	 * Note that the image needs to be a .png document.
	 */
	
	private String name;
	private long id;
	
	public Player(String player) {
		super(player);
		Gdx.input.setInputProcessor(this);
	}

	
	/**
	 * This method returns the name of the player.
	 * @return name
	 * */
	
	public String getName() {
		return name;
	}

	
	/**
	 * @param name
	 * This method requires the name of the player.
	 * */
	
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the player id
	 */
	public long getId() {
		return id;
	}


	/**
	 * @param requires the player id
	 */
	public void setId(long id) {
		this.id = id;
	}


	/* (non-Javadoc)
	 * @see com.badlogic.gdx.InputProcessor#keyDown(int)
	 */
	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}


	/* (non-Javadoc)
	 * @see com.badlogic.gdx.InputProcessor#keyUp(int)
	 */
	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}


	/* (non-Javadoc)
	 * @see com.badlogic.gdx.InputProcessor#keyTyped(char)
	 */
	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}


	/* (non-Javadoc)
	 * @see com.badlogic.gdx.InputProcessor#touchDown(int, int, int, int)
	 */
	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}


	/* (non-Javadoc)
	 * @see com.badlogic.gdx.InputProcessor#touchUp(int, int, int, int)
	 */
	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}


	/* (non-Javadoc)
	 * @see com.badlogic.gdx.InputProcessor#touchDragged(int, int, int)
	 */
	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}


	/* (non-Javadoc)
	 * @see com.badlogic.gdx.InputProcessor#mouseMoved(int, int)
	 */
	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}


	/* (non-Javadoc)
	 * @see com.badlogic.gdx.InputProcessor#scrolled(int)
	 */
	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
	
}

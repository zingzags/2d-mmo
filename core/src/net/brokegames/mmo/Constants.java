/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.mmo;

/**
 * @author Steven
 *
 */
public class Constants {
	
	public static final String gameName = "Project Zen";
	public static final int width = 384;
	public static final int height = 512;
	public static final boolean fullscreen = false;
	public static final boolean resizable = true;
	public static final float gravityX = 0f;
	public static final float gravityY = -110f; //This means we are falling at a rate of -98 units on the y-axis
	public static final int pixelsToMeters = 10;
}

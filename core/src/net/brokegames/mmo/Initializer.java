package net.brokegames.mmo;

import com.badlogic.ashley.core.Engine;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import net.brokegames.handlers.GameEngine;
import net.brokegames.handlers.MapHandler;
import net.brokegames.handlers.maps.LevelCollisionGenerator;

public class Initializer extends ApplicationAdapter {
	
	private SpriteBatch batch;
	private static GameEngine engine;
	private static World world;
	private static OrthographicCamera camera;
	private TiledMapRenderer mapRenderer;
	private ExtendViewport viewport;
	
	@Override
	public void create() {
        // Create a physics world, the heart of the simulation. The Vector passed in is gravity
		world = new World(new Vector2(Constants.gravityX, Constants.gravityY), true); // We are applying "gravity" to a map	
		MapHandler.loadLevel("Test");	
		LevelCollisionGenerator level = new LevelCollisionGenerator(world);
		level.createPhysics(MapHandler.getTiledMap());
		batch = new SpriteBatch();
		engine = new GameEngine(new Engine(), batch, world);
		camera = new OrthographicCamera();
		camera.setToOrtho(false, Constants.width, Constants.height);
		camera.update();
		viewport = new ExtendViewport(Constants.width, Constants.height, camera);
		mapRenderer = new OrthogonalTiledMapRenderer(MapHandler.getTiledMap());
		viewport.apply();
	}

	@Override
	public void render() {
		Gdx.graphics.setTitle(Constants.gameName + " FPS:" + Gdx.graphics.getFramesPerSecond() + " "+ camera.position);
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glBlendFunc(GL30.GL_SRC_ALPHA, GL30.GL_ONE_MINUS_SRC_ALPHA);
		Gdx.gl.glEnable(GL30.GL_BLEND);
        /* Advance the world, by the amount of time that has elapsed since the last frame
           Generally in a real game, dont do this in the render loop, as you are tying the physics
           update rate to the frame rate, and vice versa*/
        world.step(Gdx.graphics.getDeltaTime(), 6, 2);
		mapRenderer.setView(camera);
		mapRenderer.render();
		batch.begin();
		engine.update();
		batch.setProjectionMatrix(camera.combined);
		batch.end();
		if (Gdx.input.isKeyPressed(Keys.W)) {
			camera.position.set(camera.position.x, ++camera.position.y, 0);	
		} else if (Gdx.input.isKeyPressed(Keys.S)) {
			camera.position.set(camera.position.x, --camera.position.y, 0);
		} else if (Gdx.input.isKeyPressed(Keys.A)) {
			camera.position.set(--camera.position.x, camera.position.y, 0);
		} else if (Gdx.input.isKeyPressed(Keys.D)) {
			camera.position.set(++camera.position.x, camera.position.y, 0);
		}
		if (camera.position.x >= MapHandler.getLvlPixelWidth()-camera.viewportWidth / 2) {
			camera.position.x = MapHandler.getLvlPixelWidth()-camera.viewportWidth / 2;
		}
		if (camera.position.x <= camera.viewportWidth / 2) {
			camera.position.x = camera.viewportWidth / 2;
		}		
		if (camera.position.y >= MapHandler.getLvlPixelHeight()-camera.viewportHeight) {
			camera.position.y = MapHandler.getLvlPixelHeight()-camera.viewportHeight;
		}
		if (camera.position.y <= camera.viewportHeight / 2) {
			camera.position.y = camera.viewportHeight / 2;
		}
		camera.update(); 
	}
	
	@Override
	public void dispose() {
		world.dispose();
		batch.dispose();
		if (MapHandler.getSound() != null)
			MapHandler.getSound().dispose();
	}
		
	@Override
	public void resize(int width, int height) {
		viewport.update(width, height, false);
	}
	
	public World getWorld() {
		return world;
	}
	
	public OrthographicCamera getCamera() {
		return camera;
	}
	
}

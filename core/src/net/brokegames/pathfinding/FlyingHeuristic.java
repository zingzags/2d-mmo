package net.brokegames.pathfinding;

import com.badlogic.gdx.ai.pfa.Heuristic;

import net.brokegames.handlers.MapHandler;

/**
 * Created by Phil on 4/24/2015.
 */
public class FlyingHeuristic implements Heuristic<Node> {
    @Override
    public float estimate(Node startNode, Node endNode) {
        int startIndex = startNode.getIndex();
        int endIndex = endNode.getIndex();

        int startY = startIndex / MapHandler.getLvlTileWidth();
        int startX = startIndex % MapHandler.getLvlTileWidth();

        int endY = endIndex / MapHandler.getLvlTileWidth();
        int endX = endIndex % MapHandler.getLvlTileWidth();

        // Pythagorean distance
        float distance = (float) Math.sqrt(Math.pow(endX - startX, 2) + Math.pow(endY - startY, 2));

        return distance;
    }
}

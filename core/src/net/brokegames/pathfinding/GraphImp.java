package net.brokegames.pathfinding;

import com.badlogic.gdx.ai.pfa.indexed.DefaultIndexedGraph;
import com.badlogic.gdx.utils.Array;

import net.brokegames.handlers.MapHandler;

/**
* Created by Phil on 3/8/2015.
*/
public class GraphImp extends DefaultIndexedGraph<Node> {
    public GraphImp() {
        super();
    }

    public GraphImp(int capacity) {
        super(capacity);
    }

    public GraphImp(Array<Node> nodes) {
        super(nodes);

        // speedier than indexOf()
        for (int x = 0; x < nodes.size; ++x) {
            nodes.get(x).index = x;
        }
    }

    public Node getNodeByXY(int x, int y) {
        int modX = x / MapHandler.getTilePixelWidth();
        int modY = y / MapHandler.getTilePixelHeight();

        return nodes.get(MapHandler.getTilePixelWidth() * modY + modX);
    }
}

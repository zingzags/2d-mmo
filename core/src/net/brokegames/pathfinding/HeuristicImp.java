package net.brokegames.pathfinding;

import com.badlogic.gdx.ai.pfa.Heuristic;

import net.brokegames.handlers.MapHandler;

/**
 * Created by Phil on 3/8/2015.
 */
public class HeuristicImp implements Heuristic<Node> {
    @Override
    public float estimate(Node startNode, Node endNode) {
        int startIndex = startNode.getIndex();
        int endIndex = endNode.getIndex();

        int startY = startIndex / MapHandler.getLvlTileWidth();
        int startX = startIndex % MapHandler.getLvlTileWidth();

        int endY = endIndex / MapHandler.getLvlTileWidth();
        int endX = endIndex % MapHandler.getLvlTileWidth();

        // magnitude of differences on both axes is Manhattan distance (not ideal)
        float distance = Math.abs(startX - endX) + Math.abs(startY - endY);

        return distance;
    }
}

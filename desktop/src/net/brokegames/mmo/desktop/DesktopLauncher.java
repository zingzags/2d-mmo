package net.brokegames.mmo.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import net.brokegames.mmo.Constants;
import net.brokegames.mmo.Initializer;

public class DesktopLauncher {
	
	public static void main(String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.fullscreen = Constants.fullscreen;
		config.resizable = Constants.resizable;
		config.useGL30 = true;
		if (!config.fullscreen) {
			config.width = Constants.width;
			config.height = Constants.height;
		}
		new LwjglApplication(new Initializer(), config);
	}
}